package ListQuanAo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.R;

import java.util.List;

public class QuanAoAdapter extends RecyclerView.Adapter<QuanAoAdapter.QuanAoViewHolder> {

    private Context context;
    private List<QuanAo> mListQuanAo;
    ItemClickListener itemClickListener;

    public QuanAoAdapter(Context context,List<QuanAo> mListQuanAo) {
        this.context = context;
        this.mListQuanAo = mListQuanAo;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public QuanAoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quan_ao, parent, false);
        return new QuanAoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuanAoAdapter.QuanAoViewHolder holder, int position) {
        QuanAo quanAo = mListQuanAo.get(position);
        if (quanAo == null) {
            return;
        }
        holder.imgQuanAo.setImageResource(quanAo.getImage());
        holder.tvNameQ.setText(quanAo.getName());
        holder.tvPriceQ.setText(quanAo.getPrice());
    }

    @Override
    public int getItemCount() {
        if (mListQuanAo != null) {
            return mListQuanAo.size();
        }
        return 0;
    }

    public class QuanAoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imgQuanAo;
        private TextView tvNameQ, tvPriceQ;

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.OnItemClick(v, getAbsoluteAdapterPosition());
            }
        }

        public QuanAoViewHolder(@NonNull View itemView) {
            super(itemView);

            imgQuanAo = itemView.findViewById(R.id.imgQuanAo);
            tvNameQ = itemView.findViewById(R.id.tvNameQ);
            tvPriceQ = itemView.findViewById(R.id.tvPriceQ);

            itemView.setOnClickListener(this);
        }


    }
    public void setOnItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void OnItemClick(View view,int position);
    }
}
