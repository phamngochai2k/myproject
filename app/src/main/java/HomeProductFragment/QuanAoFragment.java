package HomeProductFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.R;

import java.util.ArrayList;
import java.util.List;

import ListGiay.Giay;
import ListGiay.GiayAdapter;
import ListQuanAo.QuanAo;
import ListQuanAo.QuanAoAdapter;

public class QuanAoFragment extends Fragment implements QuanAoAdapter.ItemClickListener {

    private RecyclerView rcvQuanAo;
    private QuanAoAdapter quanAoAdapter;

    public QuanAoFragment(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quan_ao, container, false);

        rcvQuanAo = view.findViewById(R.id.rcv_quanao);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rcvQuanAo.setLayoutManager(gridLayoutManager);

        quanAoAdapter = new QuanAoAdapter(getActivity(),getListQuanAo());
        quanAoAdapter.setOnItemClick(this);
        rcvQuanAo.setAdapter(quanAoAdapter);

        return view;
    }


    private List<QuanAo> getListQuanAo() {
        List<QuanAo> list = new ArrayList<>();


        list.add(new QuanAo(R.drawable.songlucfc, "Áo VIP Sông Lục FC", "139.000.000 VNĐ"));
        list.add(new QuanAo(R.drawable.realhong, "Real Madrid (Hồng)", "139.000 VNĐ"));
        list.add(new QuanAo(R.drawable.realtrang, "Real Madrid (Trắng)", "139.000 VNĐ"));
        list.add(new QuanAo(R.drawable.psgtrang, "Paris Saint German (Trắng)", "139.000 VNĐ"));
        list.add(new QuanAo(R.drawable.mancityxanh, "Manchester City (Xanh)", "139.000 VNĐ"));
        list.add(new QuanAo(R.drawable.kamitowhite, "Kamito Trắng", "299.000 VNĐ"));
        list.add(new QuanAo(R.drawable.kamitobluejpg, "Kamito Xanh", "299.000 VNĐ"));
        list.add(new QuanAo(R.drawable.kamitoblack, "Kamito Đen", "499.000 VNĐ"));
        list.add(new QuanAo(R.drawable.haglorangejpg, "HAGL Vàng", "499.000 VNĐ"));
        list.add(new QuanAo(R.drawable.haglwhite, "HAGL Trắng", "499.000 VNĐ"));

        return list;
    }


    @Override
    public void OnItemClick(View view, int position) {
        Toast.makeText(requireContext(), ""+position, Toast.LENGTH_SHORT).show();
    }
}