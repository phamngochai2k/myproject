package HomeProductFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import NavigationBottomFragment.CartFragment;
import NavigationBottomFragment.HomeFragment;
import NavigationBottomFragment.UserFragment;

public class HomeOrderViewPagerAdapter extends FragmentStatePagerAdapter {
    public HomeOrderViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new GiayFragment();
            case 1:
                return new QuanAoFragment();
            case 2:
                return new PhuKienFragment();
            default:
                return new GiayFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Giày";
            case 1:
                return "Quần áo";
            case 2:
                return "Phụ kiện";
            default:
                return "Giày";
        }
    }
}
