package HomeProductFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.R;

import java.util.ArrayList;
import java.util.List;

import ListGiay.Giay;
import ListGiay.GiayAdapter;
import ListPhuKien.PhuKien;
import ListPhuKien.PhuKienAdapter;
import ListQuanAo.QuanAoAdapter;

public class PhuKienFragment extends Fragment implements PhuKienAdapter.ItemClickListener {
    private RecyclerView rcvPhuKien;
    private PhuKienAdapter phuKienAdapter;

    public PhuKienFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phu_kien, container, false);

        rcvPhuKien = view.findViewById(R.id.rcv_phu_kien);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rcvPhuKien.setLayoutManager(gridLayoutManager);

        phuKienAdapter = new PhuKienAdapter(getActivity(),getListPhuKien());
        phuKienAdapter.setOnItemClick(this);

        rcvPhuKien.setAdapter(phuKienAdapter);

        return view;
    }


    private List<PhuKien> getListPhuKien() {
        List<PhuKien> list = new ArrayList<>();
        list.add(new PhuKien(R.drawable.bangkeo, "Băng keo thể thao", "2.999.000 VNĐ"));
        list.add(new PhuKien(R.drawable.bongdongluc, "Bóng động lực", "690.000 VNĐ"));
        list.add(new PhuKien(R.drawable.coluuniem, "Cờ lưu niệm", "690.000 VNĐ"));
        list.add(new PhuKien(R.drawable.cup, "Cúp", "690.000 VNĐ"));
        list.add(new PhuKien(R.drawable.hccauvong, "Huy chương cầu vồng", "2.999.000"));

        list.add(new PhuKien(R.drawable.mizunowakeup, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.mizunowakeup, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.mizunowakeup, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.mizunowakeup, "Mizuno WakeUp", "2.999.000"));

        list.add(new PhuKien(R.drawable.haglwhite, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.kamitoblack, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.kamitobluejpg, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.velocidad, "Mizuno WakeUp", "2.999.000"));
        list.add(new PhuKien(R.drawable.kamitowhite, "Kamito Trắng", "2.999.000"));


        return list;
    }

    @Override
    public void OnItemClick(View view, int position) {
        Toast.makeText(requireContext(), ""+position, Toast.LENGTH_SHORT).show();
    }
}