package HomeProductFragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myproject.R;

import java.util.ArrayList;
import java.util.List;

import ListGiay.Giay;
import ListGiay.GiayAdapter;

public class GiayFragment extends Fragment implements  GiayAdapter.ItemClickListener{

    private RecyclerView rcvGiay;
    private GiayAdapter giayAdapter;
    private FrameLayout fg_detail;
    private View mView;
    private ImageView imgGiay;
    private TextView tvName, tvPrice;

    public GiayFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_giay, container, false);

        rcvGiay = mView.findViewById(R.id.rcv_giay);
        fg_detail = mView.findViewById(R.id.fg_detail);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rcvGiay.setLayoutManager(gridLayoutManager);

        giayAdapter = new GiayAdapter(getActivity(), getListGiay());
        giayAdapter.setOnItemClick(this);
        rcvGiay.setAdapter(giayAdapter);

        return mView;
    }

    private List<Giay> getListGiay() {
        List<Giay> list = new ArrayList<>();

        list.add(new Giay(R.drawable.kamitoblack, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitobluejpg, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.velocidad, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitowhite, "Kamito Trắng", "2.999.000"));
        list.add(new Giay(R.drawable.ta11white, "Mizuno WakeUp", "690.000 VNĐ"));
        list.add(new Giay(R.drawable.ta11blue, "Tuấn Anh 11 Xanh", "690.000 VNĐ"));
        list.add(new Giay(R.drawable.ta11red, "Tuấn Anh 11 Đỏ", "690.000 VNĐ"));
        list.add(new Giay(R.drawable.kamitoblack, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitobluejpg, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.velocidad, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitowhite, "Kamito Trắng", "2.999.000"));

        list.add(new Giay(R.drawable.kamitoblack, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitobluejpg, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.velocidad, "Mizuno WakeUp", "2.999.000"));
        list.add(new Giay(R.drawable.kamitowhite, "Kamito Trắng", "2.999.000"));

        return list;
    }

    @Override
    public void OnItemClick(View view, int position) {
//        fg_detail.setVisibility(View.VISIBLE);
        Toast.makeText(requireContext(),""+position,Toast.LENGTH_SHORT).show();

    }
}