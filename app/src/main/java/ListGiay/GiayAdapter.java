package ListGiay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.R;

import java.util.List;

public class GiayAdapter extends RecyclerView.Adapter<GiayAdapter.GiayViewHolder> {

    private Context context;
    private List<Giay> mListGiay;
    private RecyclerView rcvGiay;
    ItemClickListener itemClickListener;

    public GiayAdapter(Context context, List<Giay> mListGiay) {
        this.context = context;
        this.mListGiay = mListGiay;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GiayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_giay, parent, false);
        return new GiayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GiayAdapter.GiayViewHolder holder, int position) {
        Giay giay = mListGiay.get(position);
        if (giay == null) {
            return;
        }
        holder.imgGiay.setImageResource(giay.getImage());
        holder.tvName.setText(giay.getName());
        holder.tvPrice.setText(giay.getPrice());

    }

    @Override
    public int getItemCount() {
        if (mListGiay != null) {
            return mListGiay.size();
        }
        return 0;
    }


    public class GiayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imgGiay;
        private TextView tvName,tvPrice;

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.OnItemClick(v, getAbsoluteAdapterPosition());
            }
        }

        public GiayViewHolder(@NonNull View itemView) {
            super(itemView);
            imgGiay = itemView.findViewById(R.id.imgGiay);
            tvName = itemView.findViewById(R.id.tvName);
            tvPrice = itemView.findViewById(R.id.tvPrice);

            itemView.setOnClickListener(this);
        }
    }

    public void setOnItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void OnItemClick(View view, int position);
    }
}
