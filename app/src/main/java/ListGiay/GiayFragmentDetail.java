package ListGiay;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myproject.R;

public class GiayFragmentDetail extends Fragment {

    private ImageView imgDetail;
    private TextView tvNameG, tvPriceG;
    private View mView;

    public GiayFragmentDetail() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_giay_detail, container, false);

        imgDetail = mView.findViewById(R.id.imgDetail);
        tvNameG = mView.findViewById(R.id.tvDetailName);
        tvPriceG = mView.findViewById(R.id.tvDetailPrice);

        return mView;
    }
}