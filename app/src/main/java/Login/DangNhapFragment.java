package Login;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.myproject.R;


public class DangNhapFragment extends Fragment implements ILogin {
    DangNhapFragment fragment;
    PresenterLogin presenterLogin;

    public static DangNhapFragment newInstance() {


        Bundle args = new Bundle();

        DangNhapFragment fragment = new DangNhapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        binding = DataBindingUtil.setContentView(this,R.layout.fragment_dang_nhap);
//
//        presenterLogin = new PresenterLogin(this);
//
//        binding.btn_sign_up.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String  username = binding.et_sdt_dang_nhap.getText().toString();
//                String  pass = binding.et_pass_dang_nhap.getText().toString();
//
//                presenterLogin.onLogin(username,pass);
//            }
//        });
//
//        return binding.getRoot();
        View view = inflater.inflate(R.layout.fragment_dang_nhap, container, false);
        return view;
    }

    @Override
    public void onSuccessFul() {
        Toast.makeText(getContext(), "Login Thành công", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMessenger(String mes) {
        Toast.makeText(getContext(), mes, Toast.LENGTH_LONG).show();
    }
}