package Login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import NavigationBottomFragment.CartFragment;
import NavigationBottomFragment.HomeFragment;
import NavigationBottomFragment.UserFragment;

public class LoginViewPagerAdapter extends FragmentStatePagerAdapter {
    public LoginViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new DangNhapFragment();
            case 1:
                return new DangKiFragment();
            default:
                return new DangNhapFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Đăng nhập";
            case 1:
                return "Đăng kí";
            default:
                return "Đăng nhập";
        }
    }
}
