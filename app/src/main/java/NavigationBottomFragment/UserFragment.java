package NavigationBottomFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.myproject.R;
import com.google.android.material.tabs.TabLayout;

import HomeProductFragment.HomeOrderViewPagerAdapter;
import Login.LoginViewPagerAdapter;

public class UserFragment extends Fragment {
    private TabLayout mtabLayout;
    private ViewPager mviewPager;
    private View mView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_user,container,false);

        mtabLayout = mView.findViewById(R.id.tab_user);
        mviewPager = mView.findViewById(R.id.view_user);

        LoginViewPagerAdapter loginViewPagerAdapter = new LoginViewPagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mviewPager.setAdapter(loginViewPagerAdapter);

        mtabLayout.setupWithViewPager(mviewPager);

        return mView;
    }
}
