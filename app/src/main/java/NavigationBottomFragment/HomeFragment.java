package NavigationBottomFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.myproject.R;
import com.google.android.material.tabs.TabLayout;

import HomeProductFragment.HomeOrderViewPagerAdapter;

public class HomeFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private View mView;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home,container,false);

        tabLayout = mView.findViewById(R.id.order_tab_layout);
        viewPager = mView.findViewById(R.id.order_view_pager);

        HomeOrderViewPagerAdapter orderViewPagerAdapter = new HomeOrderViewPagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setAdapter(orderViewPagerAdapter);

        tabLayout.setupWithViewPager(viewPager);
        return mView;
    }
}
