package ListPhuKien;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myproject.R;

import java.util.List;

import ListGiay.GiayAdapter;


public class PhuKienAdapter extends RecyclerView.Adapter<PhuKienAdapter.PhuKienViewHolder> {

    private Context context;
    private List<PhuKien> mListPhuKien;
    RecyclerView rcvPhuKien;
    ItemClickListener itemClickListener;

    public PhuKienAdapter(Context context, List<PhuKien> mListPhuKien) {
        this.context = context;
        this.mListPhuKien = mListPhuKien;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public PhuKienViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_phu_kien, parent, false);
        return new PhuKienViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhuKienAdapter.PhuKienViewHolder holder, int position) {
        PhuKien phuKien = mListPhuKien.get(position);
        if (phuKien == null) {
            return;
        }

        holder.imgPhuKien.setImageResource(phuKien.getImgPhuKien());
        holder.tvName.setText(phuKien.getTvName());
        holder.tvPrice.setText(phuKien.getTvPrice());
    }

    @Override
    public int getItemCount() {
        if (mListPhuKien != null) {
            return mListPhuKien.size();
        }
        return 0;
    }

    public class PhuKienViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imgPhuKien;
        private TextView tvName, tvPrice;

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.OnItemClick(v, getAbsoluteAdapterPosition());
            }
        }

        public PhuKienViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhuKien = itemView.findViewById(R.id.imgPhuKien);
            tvName = itemView.findViewById(R.id.tvNameP);
            tvPrice = itemView.findViewById(R.id.tvPriceP);

            itemView.setOnClickListener(this);
        }


    }

    public void setOnItemClick(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void OnItemClick(View view, int position);
    }

}
