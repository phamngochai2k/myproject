package ListPhuKien;

public class PhuKien {
    private int imgPhuKien;
    private String tvName, tvPrice;

    public PhuKien(int imgPhuKien, String tvName, String tvPrice) {
        this.imgPhuKien = imgPhuKien;
        this.tvName = tvName;
        this.tvPrice = tvPrice;
    }

    public int getImgPhuKien() {
        return imgPhuKien;
    }

    public void setImgPhuKien(int imgPhuKien) {
        this.imgPhuKien = imgPhuKien;
    }

    public String getTvName() {
        return tvName;
    }

    public void setTvName(String tvName) {
        this.tvName = tvName;
    }

    public String getTvPrice() {
        return tvPrice;
    }

    public void setTvPrice(String tvPrice) {
        this.tvPrice = tvPrice;
    }
}
